import "./table.css"

const TodoTable = (props) =>{
    const {userData} = props
    const {id,title,completed} = userData
    const isCompeted = completed ? "Yes" : "No"

    console.log(id);
    return(
        <tr>
            <td>{id}</td>
            <td>{title}</td>
            <td>{isCompeted}</td>
        </tr>
    )
}

export default TodoTable