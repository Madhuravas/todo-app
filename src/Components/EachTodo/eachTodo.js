import { useParams} from "react-router-dom";


import EachTodoItem from "./eachTodoItem";

const EachTodo = () =>{
   const {id} = useParams()
   return(
        <EachTodoItem todoId={id}/>
   )
}


export default EachTodo