import { Component } from "react";

class EachTodoItem extends Component{
    state = {eachTodo:""}

    componentDidMount(){
        const {todoId} = this.props
        
        fetch(`https://jsonplaceholder.typicode.com/todos/${todoId}`)
        .then(response =>{
            return response.json()
        }).then(data =>{
            this.setState({eachTodo:data})
        })
    }

    render(){
        const {eachTodo} = this.state
        if(eachTodo !== ""){
            return(
                <div className="each-todo-card">
                <h1>Todo of Id:{eachTodo.id} </h1>
                <h2>{eachTodo.title}</h2>
                </div>
            )
        }
        
    }
}

export default EachTodoItem