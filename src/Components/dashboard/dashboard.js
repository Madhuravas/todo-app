import { Component } from "react";
import { Link } from "react-router-dom";


import TodoTable from "../todosDataTables/table";
import "./dashboard.css"

class Dashboard extends Component {

    state = { userTodos: "" }

    componentDidMount() {
        fetch("https://jsonplaceholder.typicode.com/todos")
            .then(response => {
                return response.json()
            }).then(data => {
                this.setState({ userTodos: data })
            })
    }



    render() {
        const { userTodos } = this.state
        const slicedUserTodos = (userTodos || []).slice(0, 10)
        return (
            <div className="table-main-card">
                <div className="table-card">
                    <Link to="/settings"><div className="helo"><i className="settings-menu fas fa-bars"></i></div></Link>
                    <div>
                        <table >
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Todo</th>
                                    <th>Completed</th>
                                </tr>
                            </thead>
                            <tbody>
                                {(slicedUserTodos || []).map(eachItem => {
                                    return <TodoTable key={eachItem.id} userData={eachItem} />
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default Dashboard