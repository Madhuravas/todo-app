import { useNavigate } from "react-router-dom";

import { useState } from "react";

import {UserLoginData} from "../../Context/logindata";

import "./login.css"




function LoginForm() {
    const navigate = useNavigate()
    
    const [data, setUserData] = useState({
        userInputName:"",
        inputPassword:"",
        isValid:true
    })


    const onChangeUserName = (event) =>{

        setUserData({...data,userInputName:event.target.value})
    }
    const onChangepassword = (event) =>{

        setUserData({...data,inputPassword:event.target.value})
    }


    return (<UserLoginData.Consumer>

        {(context) => {
            const { userName, password } = context
        

            const inputSubmit = (event) => {
                event.preventDefault()
                if (data.inputPassword === password && data.userInputName === userName) {
                    navigate("/dashboard")
                } else {
                    setUserData({...data,isValid:false})
                    console.log(data)
                }
                
            }

            return (
                <div className="form-bg-card">
                    <form onSubmit={inputSubmit} className="form-card">
                        <h1>Welcome </h1>
                        <div>
                            <label className="label-text" htmlFor="name">User name</label>
                            <br />
                            <input className="input" id="name" type="text" onChange={onChangeUserName} placeholder="Enter name"/>
                        </div>
                        <div>
                            <label className="label-text" htmlFor="password">Password</label>
                            <br />
                            <input className="input" id="password" type="password" onChange={onChangepassword} placeholder="Enter Password"/>
                        </div>
                        {data.isValid? "" :<span className="error-msg">Invalid data</span>  }
                        
                        <button type="submit" className="button">Log in</button>
                    </form>
                </div>
            )
        }}
    </UserLoginData.Consumer>
    )

}

export default LoginForm