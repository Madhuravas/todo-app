import { Component } from "react";
import "./displaySearchTodos.css"

class DisplaySearchTodos extends Component {
    state = { searchTodosData: "" }

    componentDidMount() {
        const idArr = this.props.searchdata
        const params = idArr.reduce((acc, eachid) => {
            acc += "id=" + eachid + "&"
            return acc
        }, "")


        fetch(`https://jsonplaceholder.typicode.com/todos/?${params}`)
            .then(res => {
                return res.json()
            }).then(data => {
                this.setState({ searchTodosData: data })
            })
    }


    render() {
        const { searchTodosData } = this.state

        if (searchTodosData !== "") {
            return (
                <div className="search-todos-card">
                <div>
                    <h1>Todos</h1>
                    <table >
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Todo</th>
                                <th>Completed</th>
                            </tr>
                        </thead>
                        <tbody>
                            {searchTodosData.map(eachTodo => {
                                return (<tr>
                                 <td>{eachTodo.id}</td>
                                 <td>{eachTodo.title}</td>
                                 <td>{eachTodo.completed?"Yes" : "No"}</td>
                             </tr>)
                                // return <p key={eachTodo.id}>{eachTodo.title}</p>
                            })}
                        </tbody>
                    </table>
                 </div>
                </div>
            )
        }
    }
}


export default DisplaySearchTodos