import { useSearchParams } from "react-router-dom"

import DisplaySearchTodos from "./displaysearchTodo"
const SearchTods = () =>{

    const [params] = useSearchParams()
    const ids= params.getAll("id")
     
    
    return(
    <DisplaySearchTodos searchdata={ids}/>
    )
}

export default SearchTods