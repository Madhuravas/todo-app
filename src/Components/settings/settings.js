import { useState } from "react"

import "./settings.css"

import {UserLoginData} from "../../Context/logindata"

const Settings = () => {

    const [data, setUserData] = useState({
        oldPassword: "",
        newPassword: "",
        newUserName: "",
        isValid:true,
        isSuccess:false
    })

    return (
        <UserLoginData.Consumer>
            {value => {
                const { password, changeData } = value
                console.log(value)
                const onUserOldpassword = (event) => {
                    
                    setUserData({...data, oldPassword:event.target.value})
                }

                const onChangeUserName = (event) => {
                
                    setUserData({...data,newUserName:event.target.value})
                }

                const onUserNewpassword = (event) => {
                
                    setUserData({...data,newPassword:event.target.value})
                }

                const onClickPassword = () => {
                    if (password === data.oldPassword) {
                        changeData(data.newPassword, data.newUserName)
                        setUserData({...data,isSuccess:true})
                    }else{
                        setUserData({...data,isValid:false})
                    }
                }

                return (
                    <div className="settings-card">
                        <div>
                            <h1>Change your details</h1>
                            <div>
                                <label className="label-text" htmlFor="old-password">Old password</label>
                                <br />
                                <input className="input" id="old-password" type="password" onChange={onUserOldpassword}></input>
                            </div>
                            <div>
                                <label className="label-text" htmlFor="new-name">New name</label>
                                <br />
                                <input className="input" id="new-name" type="text" onChange={onChangeUserName}></input>
                            </div>
                            <div>
                                <label className="label-text" htmlFor="new-password">New password</label>
                                <br />
                                <input className="input" id="new-password" type="password" onChange={onUserNewpassword}></input>
                            </div>
                            {data.isSuccess ? <span className="success-msg">User Data updated successfully</span> : ""}
                            {data.isValid ? "" : <span className="error-msg">Invalid password</span>}
                            <br/>
                            <button onClick={onClickPassword} className="button">Change</button>
                        </div>
                    </div>
                )

            }}

        </UserLoginData.Consumer>
    )
}


export default Settings