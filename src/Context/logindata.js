import React, { createContext, Component } from "react";




export const UserLoginData = createContext()

class UserLoginDataProvider extends Component {
    state = {
        userName: "Maxwell",
        password: "123456",
        
    }

    changeData = (newPassword,newUserName) => {
        this.setState({ password: newPassword ,userName: newUserName})
      }

    render(){
        return(
            <UserLoginData.Provider value={{...this.state, changeData:this.changeData}}>
                {this.props.children}
            </UserLoginData.Provider>
        )
    }
}

export default UserLoginDataProvider




