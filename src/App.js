import { Component } from "react";

import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import LoginForm from './Components/loginForm/login';
import Dashboard from "./Components/dashboard/dashboard";
import Settings from "./Components/settings/settings";
import NotFound from "./Components/notFound/notFound";
import EachTodo from "./Components/EachTodo/eachTodo";
import SearchTods from "./Components/searchTods/searchTods";

import './App.css';
import UserLoginDataProvider from "./Context/logindata";

class App extends Component {
 
  render() {
    return (
      <BrowserRouter>
        <UserLoginDataProvider>
          <Routes>
            <Route path="/" element={<LoginForm />} />
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/todos/:id" element={<EachTodo/>} />
            <Route path="/todos" element={<SearchTods/>} />
            <Route path="/settings" element={< Settings />} />
            <Route path="*" element={<NotFound/>}/>
          </Routes>
        </UserLoginDataProvider>
      </BrowserRouter>
    );
  }
}

export default App;
